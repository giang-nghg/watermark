from __future__ import absolute_import, unicode_literals
from celery import shared_task

from watermark.serializers import WatermarkSerializer


@shared_task
def create_watermark(data):
    serializer = WatermarkSerializer(data=data)
    serializer.is_valid()
    serializer.save()

    return serializer.data
