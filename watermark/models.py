from django.db.models import Model, TextField
from django.core.exceptions import ValidationError


CONTENT_TYPES = [
    ('book', 'Book'),
    ('journal', 'Journal')
]
TOPICS = [
    ('business', 'Business'),
    ('science', 'Science'),
    ('media', 'Media'),
]


class Watermark(Model):
    content = TextField(choices=CONTENT_TYPES)
    title = TextField()
    author = TextField()
    topic = TextField(choices=TOPICS, blank=True)

    def save(self, *args, **kwargs):
        self.full_clean()
        return super().save(*args, **kwargs)

    def clean(self):
        if self.content == 'journal' and len(self.topic) > 0:
            raise ValidationError('Journal should not have topic')
