from rest_framework.serializers import ModelSerializer

from watermark.models import Watermark


class WatermarkSerializer(ModelSerializer):
    class Meta:
        model = Watermark
        fields = ('id', 'content', 'title', 'author', 'topic')
