from unittest.mock import Mock, patch
from django.test import TestCase

from watermark.tasks import create_watermark
from watermark.models import Watermark


class CreateWatermarkTaskTestCase(TestCase):
    def setUp(self):
        self.test_data = {
            'content': 'book',
            'author': 'any',
            'title': 'any',
            'topic': 'media',
        }

    def test_task_call_watermark_serializer_save_with_suppied_data(self):
        mock_serializer = Mock()
        with patch('watermark.tasks.WatermarkSerializer', return_value=mock_serializer) as mock_serializer_class:
            create_watermark(self.test_data)

        mock_serializer_class.assert_called_once_with(data=self.test_data)
        mock_serializer.save.assert_called_once_with()
