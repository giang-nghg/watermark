from django.test import TestCase
from django.core.exceptions import ValidationError

from watermark.models import Watermark


class WatermarkModelTestCase(TestCase):
    def test_validate_model_should_not_have_topic(self):
        watermark = Watermark(
            content='journal',
            topic='any',
            author='any',
            title='any'
        )

        self.assertRaises(ValidationError, watermark.save)
