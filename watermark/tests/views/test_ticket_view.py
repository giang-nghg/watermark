from unittest.mock import patch, Mock

from django.test import TestCase, Client


class TicketViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

    def test_get_ticket_return_ticket_status_if_ticket_is_not_finished(self):
        mock_result = Mock(result=None, status='some_status')

        with patch('watermark.views.AsyncResult', return_value=mock_result):
            resp = self.client.get('/ticket/some_ticket_id/')
        data = resp.json()

        self.assertEqual(data['status'], mock_result.status)

    def test_get_ticket_return_ticket_result_if_ticket_is_finished(self):
        mock_result = Mock(result='any json serializable result')

        with patch('watermark.views.AsyncResult', return_value=mock_result):
            resp = self.client.get('/ticket/some_ticket_id/')
        data = resp.json()

        self.assertEqual(data, mock_result.result)
