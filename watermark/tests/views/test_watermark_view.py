from unittest.mock import patch, Mock
import uuid
from django.test import TestCase, Client
from django.core.exceptions import ValidationError

from watermark.models import Watermark


class WatermarkViewTestCase(TestCase):
    def setUp(self):
        self.client = Client()

        self.request_data = {
            'title': 'any',
            'author': 'any',
            'content': 'book',
            'topic': 'any'
        }

    def test_post_watermark_call_create_watermark_task_with_request_data(self):
        task_id = str(uuid.uuid4())
        mock_task = Mock(id=task_id)
        with patch('watermark.views.create_watermark.delay', return_value=mock_task) as mock_task_call:
            resp = self.client.post('/watermark/', self.request_data)

        mock_task_call.assert_called_once_with(self.request_data)

    def test_post_watermark_return_task_id_as_ticket_id(self):
        task_id = str(uuid.uuid4())
        mock_task = Mock(id=task_id)
        with patch('watermark.views.create_watermark.delay', return_value=mock_task):
            resp = self.client.post('/watermark/', self.request_data)
        data = resp.json()

        self.assertEqual(data['ticket_id'], task_id)

    def test_post_watermark_return_bad_request_if_validation_error_happen(self):
        error_message = 'some error'
        with patch('watermark.views.Watermark.clean', side_effect= ValidationError(error_message)):
            resp = self.client.post('/watermark/', self.request_data)
        data = resp.json()

        self.assertEqual(resp.status_code, 400)
        self.assertEqual(data['detail'], error_message)
