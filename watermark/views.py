from rest_framework.viewsets import ModelViewSet, ReadOnlyModelViewSet
from rest_framework.response import Response
from rest_framework import status
from celery.result import AsyncResult
from django.core.exceptions import ValidationError

from watermark.models import Watermark
from watermark.serializers import WatermarkSerializer
from watermark.tasks import create_watermark


class WatermarkViewSet(ModelViewSet):
    serializer_class = WatermarkSerializer
    queryset = Watermark.objects.all()

    def create(self, request, *args, **kwargs):
        data = request.data.dict()

        validator = Watermark(**data)
        try:
            validator.clean()
        except ValidationError as e:
            return Response({
                'detail': e.message
            }, status=status.HTTP_400_BAD_REQUEST)

        task = create_watermark.delay(data)

        return Response({
            'ticket_id': task.id
        }, status=status.HTTP_201_CREATED)


class TicketViewSet(ReadOnlyModelViewSet):
    def retrieve(self, request, pk, *args, **kwargs):
        task = AsyncResult(pk)
        if not task.result:
            data = {
                'status': task.status
            }
        else:
            data = task.result

        return Response(data)
