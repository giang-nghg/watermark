#!/usr/bin/env bash

export DB_PASSWORD=$(cat /run/secrets/writer_password)

chmod +x wait-for-it.sh
./wait-for-it.sh msg_broker:5672 -s -t 60 -- celery -A watermark worker -l info &
./wait-for-it.sh db:5432 -s -t 60 -- python manage.py migrate && python manage.py runserver 0.0.0.0:8000
